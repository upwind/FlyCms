package com.flycms.constant;

/**
 * Open source house, All rights reserved
 * 开发公司：28844.com<br/>
 * 版权：开源中国<br/>
 */
public class ScoreRuleConsts {

    /**
     * 注册初始化奖励
     */
    public static final String REG_INIT = "a9d8977a6a8e4734bae0d0378faa4b60";

    /**
     * 邮箱认证
     */
    public static final String EMAIL_AUTHENTICATION = "276948bf39ac4241a4b0a71daf045634";

    /**
     * 登录账号
     */
    public static final String LOGIN = "7cabb8204a51419899df67617e8ac7e6";


}
