package com.flycms.module.article.service;

import com.flycms.core.entity.DataVo;
import com.flycms.core.entity.PageVo;
import com.flycms.module.article.dao.ArticleDao;
import com.flycms.module.article.model.Article;
import com.flycms.module.article.model.ArticleComment;
import com.flycms.module.article.model.ArticleCount;
import com.flycms.module.config.service.ConfigService;
import com.flycms.module.search.service.SolrService;
import com.flycms.module.topic.model.Topic;
import com.flycms.module.topic.service.TopicService;
import com.flycms.module.user.service.FeedService;
import com.flycms.module.user.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Open source house, All rights reserved
 * 开发公司：28844.com<br/>
 * 版权：开源中国<br/>
 *
 * 文章内容处理服务类
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:23 2018/7/13
 */
@Service
public class ArticleService {
    @Autowired
    protected ArticleDao articleDao;
    @Autowired
    private ConfigService configService;
    @Autowired
    private SolrService solrService;
    @Autowired
    protected FeedService feedService;
    @Autowired
    protected UserService userService;
    @Autowired
    protected TopicService topicService;
    // ///////////////////////////////
    // /////      增加        ////////
    // ///////////////////////////////
    //添加文章
    public DataVo addArticle(Article article) throws ParseException {
        DataVo data = DataVo.failure("操作失败");
        if(StringUtils.isBlank(article.getTitle())){
            return data=DataVo.failure("标题不能为空！");
        }
        if(StringUtils.isBlank(article.getContent())){
            return data=DataVo.failure("内容不能为空！");
        }
        if(StringUtils.isBlank(article.getCategoryId())){
            return data=DataVo.failure("必须选择文章分类！");
        }
        if(this.checkArticleByTitle(article.getTitle(),article.getUserId(),null)){
            return data=DataVo.failure("标题已存在！");
        }
        if (StringUtils.isBlank(article.getTags())) {
            return DataVo.failure("话题不能为空，最少添加一个");
        }
        String[] tags = article.getTags().split(","); //转换为数组
        if (tags.length>5) {
            return DataVo.failure("话题数不能大于5个");
        }
        article=this.addArticle(article,tags);
        //索引本条信息
        if (article.getStatus() == 1) {
            solrService.indexArticleId(article.getId());
        }
        //更新
        //this.weight(article,null);
        data = DataVo.jump("文章添加成功！","/article/"+article.getId());
        return data;
    }

    @Transactional
    public Article addArticle(Article article,String[] tags)  throws ParseException {
        //转换为数组
        String[] str = article.getCategoryId().split(",");
        article.setTitle(StringEscapeUtils.escapeHtml4(article.getTitle()));
        article.setCreateTime(new Date());
        article.setStatus(Integer.parseInt(configService.getStringByKey("user_article_verify")));
        int totalCount=articleDao.addArticle(article);
        if(totalCount > 0) {
            articleDao.addArticleAndCategory(article.getId(), article.getCategoryId(), Integer.valueOf(str[str.length - 1]));
            //添加用户feed信息
            feedService.addUserFeed(article.getUserId(), 1, article.getId());
            //添加文章统计关联数据
            articleDao.addArticleCount(article.getId());
            if (!StringUtils.isBlank(article.getTags())) {
                for (String string : tags) {
                    if (string != " " && string.length() >= 2) {
                        Topic tag = topicService.findTopicByTopic(string);
                        if (!topicService.checkTopicByTopic(string)) {
                            Topic addtag = topicService.addTopic(string, "", 0, 1, 0, 1);
                            topicService.addTopicAndInfo(article.getId(), addtag.getId(), 1);
                        } else {
                            topicService.updateTopicByCount(tag.getId());
                            topicService.addTopicAndInfo(article.getId(), tag.getId(), 1);
                        }
                    }
                }
            }
            userService.updateArticleCount(article.getUserId());
        }
        return article;
    }


    //添加文章评论内容
    @Transactional
    public DataVo addArticleComment(ArticleComment articleComment) throws ParseException {
        DataVo data = DataVo.failure("操作失败");
        if(this.checkArticleComment(articleComment.getArticleId(),articleComment.getUserId(),articleComment.getContent())){
            return data=DataVo.failure("请勿重复替提交相同评论！");
        }
        articleComment.setCreateTime(new Date());
        int totalCount=articleDao.addArticleComment(articleComment);
        if(totalCount > 0){
            //更新文章被评论的数量
            articleDao.updateArticleCount(articleComment.getArticleId());
            data = DataVo.success("评论内容成功提交", DataVo.NOOP);
        }else{
            data=DataVo.failure("提交评论发生未知错误！");
        }
        return data;
    }
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    @Transactional
    public DataVo deleteArticleById(Integer id) {
        DataVo data = DataVo.failure("操作失败");
        Article article=articleDao.findArticleById(id,0);
        if(article==null){
            data=DataVo.failure("该信息不存在！");
        }
        articleDao.deleteArticleById(id);
        articleDao.deleteArticleAndCcategoryById(id);
        feedService.deleteUserFeed(article.getUserId(),1,article.getId());
        solrService.indexDeleteInfo(1,article.getId());
        userService.updateArticleCount(article.getUserId());
        data = DataVo.jump("删除成功！","/admin/article/article_list");
        return data;
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    //修改文章
    @Transactional
    public DataVo editArticleById(Article article) throws ParseException {
        DataVo data = DataVo.failure("操作失败");
        if(StringUtils.isBlank(article.getTitle())){
            return data=DataVo.failure("标题不能为空！");
        }
        if(StringUtils.isBlank(article.getContent())){
            return data=DataVo.failure("内容不能为空！");
        }
        if(StringUtils.isBlank(article.getCategoryId())){
            return data=DataVo.failure("必须选择文章分类！");
        }
        if(this.checkArticleByTitle(article.getTitle(),article.getUserId(),article.getId())){
            return data=DataVo.failure("标题已存在！");
        }
        if (StringUtils.isBlank(article.getTags())) {
            return DataVo.failure("话题不能为空，最少添加一个");
        }
        String[] tags = article.getTags().split(","); //转换为数组
        if (tags.length>5) {
            return DataVo.failure("话题数不能大于5个");
        }
        //转换为数组
        String[] str = article.getCategoryId().split(",");
        article.setUpdateTime(new Date());
        article.setStatus(Integer.parseInt(configService.getStringByKey("user_article_verify")));
        int totalCount=articleDao.editArticleById(article);
        if(totalCount > 0){
            //修改分类信息
            articleDao.editArticleAndCcategoryById(article.getCategoryId(),Integer.valueOf(str[str.length-1]),article.getId());

            //索引本条信息
            if(article.getStatus()==1){
                solrService.indexArticleId(article.getId());
            }else{
                solrService.indexDeleteInfo(1,article.getId());
            }
            if (!StringUtils.isBlank(article.getTags())) {
                topicService.deleteTopicAndInfoUpCount(1,article.getId());
                for (String string : tags) {
                    if (string != " " && string.length()>=2) {
                        Topic tag=topicService.findTopicByTopic(string);
                        if(!topicService.checkTopicByTopic(string)){
                            Topic addtag=topicService.addTopic(string,"",0,1,0,1);
                            topicService.addTopicAndInfo(article.getId(),addtag.getId(),1);
                        }else{
                            topicService.updateTopicByCount(tag.getId());
                            topicService.addTopicAndInfo(article.getId(),tag.getId(),1);
                        }
                    }
                }
            }
            userService.updateArticleCount(article.getUserId());
            data = DataVo.jump("文章更新成功！","/article/"+article.getId());
        }else{
            data=DataVo.failure("更新失败！");
        }
        return data;
    }

    /**
     * 按id更新文章审核状态
     *
     * @param id
     *         问题id
     * @param status
     *         0未审核 1正常状态 2审核未通过 3删除
     * @return
     */
    @Transactional
    public DataVo updateArticleStatusById(Integer id, Integer status) throws Exception {
        DataVo data = DataVo.failure("该信息不存在或已删除");
        Article article=articleDao.findArticleById(id,0);
        if(article==null){
            return data = DataVo.failure("该信息不存在或已删除");
        }
        articleDao.updateArticleStatusById(id,status);
        if(status == 1){
            //添加用户feed信息,如果存在在修改状态为1
            if(feedService.checkUserFeed(article.getUserId(),1,article.getId())){
                feedService.addUserFeed(article.getUserId(),1,article.getId());
            }else{
                feedService.updateuUserFeedById(1,article.getId(),1);
            }
            userService.updateArticleCount(article.getUserId());
            solrService.indexQuestionId(article.getId());
            //更新权重
            //this.weight(article,null);
        }else{
            //修改用户feed信息
            feedService.updateuUserFeedById(1,article.getId(),0);
            //更新用户问答数量
            userService.updateArticleCount(article.getUserId());
            //删除索引
            solrService.indexDeleteInfo(1,article.getId());
        }
        data=DataVo.success("审核操作成功！");
        return data;
    }

    /**
     * 更新文章被评论的权重分值
     *
     * @param weight
     *         权重分值
     * @param articleId
     *         文章id
     * @return
     */
    public int updateArticleWeight(Double weight,Integer articleId){
        return articleDao.updateArticleWeight(weight,articleId);
    }

    /**
     * 按id更新文章浏览数量统计
     *
     * @param articleId
     *         文章id
     * @return
     */
    public int updateArticleViewCount(Integer articleId){
        return articleDao.updateArticleViewCount(articleId);
    }
    // ///////////////////////////////
    // /////       查询       ////////
    // ///////////////////////////////
    /**
     * 按id查询文章信息
     *
     * @param id
     *         文章id
     * @param status
     *         0所有，1未审核 2正常状态 3审核未通过 4删除
     * @return
     */
    @Cacheable(value = "article")
    public Article findArticleById(Integer id, Integer status){
        return articleDao.findArticleById(id,status);
    }

    /**
     * 按id查询文章信息
     *
     * @param articleId
     *         需要查询的文章id
     * @return
     */
    public ArticleCount findArticleCountById(Integer articleId){
        return articleDao.findArticleCountById(articleId);
    }

    /**
     * 查询文章标题是否存在
     *
     * @param title
     *         发布文章标题
     * @param userId
     *         用户id，可设置为null
     * @param id
     *         当修改内容检查重复标题时，排除当前文章id，不排除可设置为null
     * @return
     */
    public boolean checkArticleByTitle(String title,Integer userId,Integer id) {
        int totalCount = articleDao.checkArticleByTitle(title,userId,id);
        return totalCount > 0 ? true : false;
    }

    /**
     * 查询文章相同的评论内容是否已添加
     *
     * @param articleId
     *         文章id
     * @param userId
     *         用户id
     * @param content
     *         评论内容
     * @return
     */
    public boolean checkArticleComment(Integer articleId,Integer userId,String content) {
        int totalCount = articleDao.checkArticleComment(articleId,userId,content);
        return totalCount > 0 ? true : false;
    }

    /**
     * 文章翻页查询
     *
     * @param title
     *         标题
     * @param createTime
     *         添加时间
     * @param pageNum
     *         当前页码
     * @param rows
     *         每页数量
     * @return
     */
    public PageVo<Article> getArticleListPage(String title, Integer userId,String createTime, Integer status,String orderby, String order, int pageNum, int rows) {
        PageVo<Article> pageVo = new PageVo<Article>(pageNum);
        pageVo.setRows(rows);
        List<Article> list = new ArrayList<Article>();
        if(orderby==null){
            orderby="id";
        }
        if(order==null){
            order="desc";
        }
        pageVo.setList(articleDao.getArticleList(title,userId,createTime,status,orderby,order,pageVo.getOffset(), pageVo.getRows()));
        pageVo.setCount(articleDao.getArticleCount(title,userId,createTime,status));
        return pageVo;
    }

    //查询文章总数
    public int getArticleIndexCount(){
        return articleDao.getArticleIndexCount();
    }

    //文章索引列表
    public List<Article> getArticleIndexList(int pageNum, int rows){
        return articleDao.getArticleIndexList(pageNum,rows);
    }

    /**
     * 文章翻页查询
     *
     * @param articleId
     *         文章id
     * @param createTime
     *         添加时间
     * @param pageNum
     *         当前页码
     * @param rows
     *         每页数量
     * @return
     */
    @Cacheable(value = "article")
    public PageVo<ArticleComment> getArticleCommentListPage(Integer articleId, Integer userId,String createTime, Integer status,String orderby, String order, int pageNum, int rows) {
        PageVo<ArticleComment> pageVo = new PageVo<ArticleComment>(pageNum);
        pageVo.setRows(rows);
        List<ArticleComment> list = new ArrayList<ArticleComment>();
        if(orderby==null){
            orderby="id";
        }
        if(order==null){
            order="desc";
        }
        pageVo.setList(articleDao.getArticleCommentList(articleId,userId,createTime,status,orderby,order,pageVo.getOffset(), pageVo.getRows()));
        pageVo.setCount(articleDao.getArticleCommentCount(articleId,userId,createTime,status));
        return pageVo;
    }

    /**
     * 按问题id查询最新的第一条评论内容
     *
     * @param articleId
     *         文章id
     * @return
     */
    public ArticleComment findNewestArticleById(Integer articleId){
        return articleDao.findNewestArticleById(articleId);
    }

    //文章索引列表
    public List<ArticleComment> getArticleCommentByArticleId(Integer articleId){
        return articleDao.getArticleCommentByArticleId(articleId);
    }
    /**
     * 计算话题的weight
     *
     * Stack Overflow热点问题的排名，与参与度(Qviews和Qanswers)和质量(Qscore和Ascores)成正比，与时间(Qage和Qupdated)成反比。
     *
     * @param article
     */
    public void weight(Article article) {
        List<ArticleComment> comments = articleDao.getArticleCommentByArticleId(article.getId());
        //浏览次数越多，就代表越受关注，得分也就越高。这里使用了以10为底的对数，用意是当访问量越来越大，它对得分的影响将不断变小。
        double Qview = 0;
        if(article.getCountView() != null && article.getCountView() > 0 ){
            Qview = Math.log10(article.getCountView());
        }
        //Qanswers表示回答的数量，代表有多少人参与这个问题。这个值越大，得分将成倍放大。这里需要注意的是，如果无人回答，Qanswers就等于0，这时Qscore再高也没用，意味着再好的问题，也必须有人回答，否则进不了热点问题排行榜。
        int Qanswer = comments.size();
        //Qscore(问题得分)= 赞成票-反对票。如果某个问题越受到好评，排名自然应该越靠前。
        int Qscore = (article.getCountDigg() == null ? 0 :article.getCountDigg()) - (article.getCountBurys() == null ? 0 :article.getCountBurys());
        Optional<Integer> Ascore = Optional.of(0);
        if (Qanswer > 0) {
            Ascore = comments.stream()
                    .map(comment -> comment.getCountDigg() - comment.getCountBurys())
                    .reduce(Integer::sum);
        }
        //也就是说，随着时间流逝，这两个值都会越变越大，导致分母增大，因此总得分会越来越小。
        //文章已发布时间
        long Qage = (new Date().getTime()-article.getCreateTime().getTime())/ (1000 * 60 * 60 * 24);
        //最新更新时间
        long Qupdated = 0;
        //查询最新更新评论时间
        ArticleComment comment = articleDao.findNewestArticleById(article.getId());
        if(comment!=null){
            Qupdated = (new Date().getTime()-comment.getCreateTime().getTime())/ (1000 * 60 * 60 * 24);
        }
        //Math.pow(((Qage / 2) + (Qupdated/2)+1), 1.5),Qage和Qupdated的单位都是天。如果一个问题的存在时间越久，或者距离上一次回答的时间越久，Qage和Qupdated的值就相应增大。
        double weightScore = ((Qview * 4) + (Qanswer * Qscore) / 5 + Ascore.get()) / Math.pow(((Qage / 2) + (Qupdated/2)+1), 1.5);
        System.out.println("==============权重计算结果：=========="+weightScore);
        articleDao.updateArticleWeight(weightScore,article.getId());
    }
}
