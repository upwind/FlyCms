package com.flycms.web.admin;

import com.flycms.core.base.BaseController;
import com.flycms.core.entity.DataVo;
import com.flycms.core.entity.PageVo;
import com.flycms.module.security.model.Permission;
import com.flycms.module.security.model.Role;
import com.flycms.module.security.service.PermissionService;
import com.flycms.module.security.service.RoleService;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: $time$ $date$
 */
@Controller
@RequestMapping("/admin/role")
public class RoleAdminController  extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(RoleAdminController.class);

    @Autowired
    protected RoleService srv;

    @Autowired
    protected PermissionService psrv;
    //添加用户权限
    @GetMapping(value = "/add")
    public String getAddRole(ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("role/add");
    }

    //处理用户组信息
    @PostMapping("/add_save")
    @ResponseBody
    public DataVo getAddRoleSave(@RequestParam(value = "name", required = false) String name){
        if (StringUtils.isBlank(name)) {
            return DataVo.failure("会员组名不能为空");
        }
        return srv.addRole(name);
    }

    //删除权限组
    @PostMapping("/del")
    @ResponseBody
    public DataVo deleteRole(@RequestParam(value = "id") int id){
        DataVo data = DataVo.failure("操作失败");
        if(id==1){
            return data = DataVo.failure("超级管理员组不能删除");
        }
        if(srv.deleteRole(id)){
            data = DataVo.success("该权限组已删除");
        }else{
            data = DataVo.failure("删除失败或者不存在！");
        }
        return data;
    }

    @GetMapping(value = "/update/{id}")
    public String updateRole(@PathVariable int id, ModelMap modelMap){
        Role role = srv.findRoleById(id);
        modelMap.put("role", role);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("role/update");
    }

    //处理用户组信息
    @PostMapping("/update_role")
    @ResponseBody
    public DataVo updateRole(@RequestParam(value = "id", required = false) String id,@RequestParam(value = "name", required = false) String name){
        if (StringUtils.isBlank(name)) {
            return DataVo.failure("会员组名不能为空");
        }
        if (!NumberUtils.isNumber(id)) {
            return DataVo.failure("参数传递错误");
        }
        return srv.updateRole(name,Integer.valueOf(id));
    }

    @GetMapping(value = "/list")
    public String roleList(@RequestParam(value = "p", defaultValue = "1") int pageNum,ModelMap modelMap){
        PageVo<Role> pageVo=srv.getRoleListPage(pageNum,20);
        modelMap.put("pageVo", pageVo);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("role/list");
    }


    @GetMapping(value = "/assignPermissions/{id}")
    public String assignPermissions(@PathVariable int id, ModelMap modelMap){
        Role role = srv.findRoleById(id);
        List<Permission> permissionList = psrv.getAllPermissions();
        LinkedHashMap<String, List<Permission>> permissionMap = psrv.groupByController(permissionList);
        modelMap.put("role", role);
        modelMap.put("permissionMap", permissionMap);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("role/assign_permissions");
    }

    @ResponseBody
    @PostMapping(value = "/markpermissions")
    public DataVo addRole(@RequestParam(value = "roleId", defaultValue = "0") Integer roleId,@RequestParam(value = "permissionId", defaultValue = "0") Integer permissionId) {
        DataVo data = DataVo.failure("操作失败");
        if(roleId==1){
            return data = DataVo.failure("超级管理员组权限不能修改");
        }
        if(roleId==0 || permissionId==0){
            return data.failure("参数错误！");
        }
        if(psrv.markAssignedPermissions(roleId,permissionId)){
            if(srv.deleteRolePermission(roleId,permissionId)){
                return data.success("删除成功！");
            }else{
                return data.failure("删除失败！");
            }
        }else{
            if(srv.addRolePermission(roleId,permissionId)){
                return DataVo.success("添加成功！");
            }else{
                return DataVo.failure("添加失败！");
            }
        }
    }
}
