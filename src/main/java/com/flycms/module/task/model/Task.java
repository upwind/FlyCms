package com.flycms.module.task.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Open source house, All rights reserved
 * 版权：28844.com<br/>
 * 开发公司：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 22:42 2018/10/25
 */
@Setter
@Getter
public class Task implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String cron;

}
