package com.flycms.web.admin;

import com.flycms.core.base.BaseController;
import com.flycms.core.entity.DataVo;
import com.flycms.module.task.service.SchedulerAllJob;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Open source house, All rights reserved
 * 版权：28844.com<br/>
 * 开发公司：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 11:47 2018/10/26
 */
@Controller
@RequestMapping("/admin/task")
public class TaskAdminController extends BaseController {
//    @Autowired
//    private TaskService taskService;

    @Autowired
    public SchedulerAllJob myScheduler;

    /**
     * 启动任务
     **/
    @ResponseBody
    @RequestMapping("/startTask")
    public DataVo startCron() throws SchedulerException {
        myScheduler.scheduleJobs();
        System.out.println("已启动任务");
        return DataVo.success("已启动任务！");
    }

    /**
     * 启此任务
     **/
    @ResponseBody
    @RequestMapping("/stopTask")
    public DataVo stopCron() throws SchedulerException {
        myScheduler.stop();
        System.out.println("已关闭任务");
        return DataVo.success("已关闭任务！");
    }

}
