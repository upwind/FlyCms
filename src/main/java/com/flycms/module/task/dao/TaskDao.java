package com.flycms.module.task.dao;

import com.flycms.module.task.model.Task;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Open source house, All rights reserved
 * 版权：28844.com<br/>
 * 开发公司：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 11:00 2018/10/26
 */
@Repository
public interface TaskDao {


    // ///////////////////////////////
    // ///// 查詢 ////////
    // ///////////////////////////////
    //按id查询分享信息
    public Task findTaskById(@Param("id") Integer id);
}
