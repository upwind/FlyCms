package com.flycms.module.task.service;

/**
 * Open source house, All rights reserved
 * 版权：28844.com<br/>
 * 开发公司：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 12:47 2018/10/26
 */
import com.flycms.module.task.dao.TaskDao;
import com.flycms.module.weight.service.WeightService;
import org.apache.commons.lang3.StringUtils;
import org.jboss.logging.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * Created by 赵亚辉 on 2017/12/18.
 */
public class ScheduledJob implements Job {
    @Autowired
    private TaskDao taskDao;
    @Autowired
    private WeightService weightService;
    @Autowired
    public SchedulerAllJob myScheduler;
    private static final Logger logger= Logger.getLogger(ScheduledJob.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException
    {
        logger.info("任务执行中......");

        weightService.updateArticleWeight();
        String cron = taskDao.findTaskById(1).getCron();
        //2.2 合法性校验.
        //System.err.println("================: " + cron);
        if (StringUtils.isEmpty(cron)) {
            System.err.println("未设置时间则停止任务调度: " + new Date());
            try {
                myScheduler.stop();
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
            // Omitted Code ..
        }
    }
}

